const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);


// function render ui
function renderListProduct(data) {
    const display_Product = $('#display__products');
    let content = '';
    data.forEach(product => {
        content += `
        <div class="product" id="${product.id}">
            <img  src="${product.img}" alt="phone">
             <div class="content__product"> 
                <h4>${product.name}</h4>
                <p>${product.price}$</p>
            </div>
        </div>
        `
    });
    display_Product.innerHTML = content;
};

// function select type
function productType(data) {
    data.forEach(product => {
        if(product.type === "iphone") {
            return typeIP.push(product);
        } else {
            return typeSS.push(product);
        }
    });
};


// function select products
const select__product = $('#selectProduct');
select__product.onchange = () => {
    if (select__product.value === 'iphone') {
        renderListProduct(typeIP);
    } else if (select__product.value === 'samsung'){
        renderListProduct(typeSS);
    } else {
        renderListProduct(products);
    }
};


