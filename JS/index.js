let products = [];
let typeIP = [];
let typeSS = [];
(function () {
  axios({
    url: `https://63e67404c8839ccc2858df76.mockapi.io/product`,
    method: "GET",
  })
    .then((res) => {
      products.push(...res.data);
      renderListProduct(res.data);
      productType(res.data);
    })
    .catch((err = "404") => {
      console.log(err);
    });
})();
