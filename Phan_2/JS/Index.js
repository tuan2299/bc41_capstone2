var dataSanPham = new dataSanPham();

function getElementById(id) {
  return document.getElementById(id);
}

function layListSanPham() {
  dataSanPham
    .layDSSP()
    .then(function (res) {
      console.log(res.data);
      renderTable(res.data);
      setLocalStorage(res.data);
    })
    .catch(function (err) {
      console.log(err);
    });
}
layListSanPham();

function setLocalStorage(mangSanPham) {
  localStorage.setItem("DSSP", JSON.stringify(mangSanPham));
}

function getLocalStorage() {
  var mangSanPham = JSON.parse(localStorage.getItem("DSSP"));
  return mangSanPham;
}

getELE("btnThemSP").addEventListener("click", function () {
  var footerEle = document.querySelector(".modal-footer");
  footerEle.innerHTML = `
        <button onclick="themSanPham()" class="btn btn-success">Add Product</button>
    `;
});

function renderTable(mangsanpham) {
  var content = "";
  var count = 1;
  mangsanpham.map(function (pd, index) {
    content += `
            <tr>
                <td>${count}</td>
                <td>${pd.tenSanPham}</td>
                <td>${pd.giaSanPham}</td>
                <td>${pd.hinhAnh}</td>
                <td>${pd.moTa}</td>
                <td>
                    <button class="btn btn-danger" onclick="xoaSanPham('${pd.id}')">Xóa</button>
                    <button class="btn btn-info" onclick="xemSanPham('${pd.id}')">Xem</button>
                </td>
            </tr>
        `;
    count++;
  });
  getElementById("tblDanhSachSP").innerHTML = content;
}

function themSanPham() {
  //B1: Lấy thông tin(info) từ form
  // data, info
  var tenSanPham = getElementById("tenSanPham").value;
  var giaSanPham = getElementById("giaSanPham").value;
  var hinhAnh = getElementById("hinhAnh").value;
  var moTa = getElementById("moTa").value;

  var pd = new sanPham(tenSanPham, giaSanPham, hinhAnh, moTa);
  console.log(pd);

  //B2: lưu info xuống database(cơ sở dữ liệu)
  dataSanPham
    .themSanPham(sanPham)
    .then(function (res) {
      //Load lại danh sách sau khi thêm thành công
      layListSanPham();

      //gọi sự kiên click có sẵn của close button
      //Để tắt modal khi thêm thành công
      document.querySelector("#myModal .close").click();
    })
    .catch(function (error) {
      console.log(error);
    });
}

function xoaSanPham(id) {
  dataSanPham
    .xoaSanPham(id)
    .then(function (res) {
      //Load lại danh sách sau khi xóa thành công
      layListSanPham();
    })
    .catch(function (err) {
      console.log(err);
    });
}

function xemSanPham(id) {
  dataSanPham
    .xemSanPham(id)
    .then(function (res) {
      console.log(res.data);
      $("#myModal").modal("show");
      document.getElementById("tenSanPham").value = res.data.tenSanPham;
      document.getElementById("giaSanPham").value = res.data.giaSanPham;
      document.getElementById("hinhAnh").value = res.data.hinhAnh;
      document.getElementById("moTa").value = res.data.moTa;
      var footerEle = document.querySelector(".modal-footer");
      footerEle.innerHTML = `
            <button onclick="capNhatSanPham('${res.data.id}')" class="btn btn-success">Update Product</button>
        `;
    })
    .catch(function (err) {
      console.log(err);
    });
}

function capNhatSanPham(id) {
  var tenSanPham = document.getElementById("tenSanPham").value;
  var giaSanPham = document.getElementById("giaSanPham").value;
  var hinhAnh = document.getElementById("hinhAnh").value;
  var moTa = document.getElementById("moTa").value;
  var pd = new sanPham(tenSanPham, giaSanPham, hinhAnh, moTa);
  console.log(pd);
  dataSanPham
    .capNhatSanPham(id, pd)
    .then(function (res) {
      console.log(res.data);
      getListProducts();
      document.querySelector("#myModal .close").click();
    })
    .catch(function (err) {
      console.log(err);
    });
}
