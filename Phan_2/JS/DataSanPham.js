const BASE_URL = "https://63e67404c8839ccc2858df76.mockapi.io/sanpham";

function dataSanPham() {
  this.layDSSP = function () {
    var product = axios({
      url: "https://63e67404c8839ccc2858df76.mockapi.io/sanpham",
      method: "GET",
    });
    return product;
  };
  this.themSanPham = function (sanpham) {
    var product = axios({
      //data: dữ liệu cần thêm vào Cơ sở dữ liệu
      url: "https://63e67404c8839ccc2858df76.mockapi.io/sanpham",
      method: "POST",
      data: sanpham,
    });
    return product;
  };
  this.xoaSanPham = function (id) {
    var product = axios({
      url: `https://63e67404c8839ccc2858df76.mockapi.io/sanpham/${id}`,
      method: "DELETE",
    });
    return product;
  };
  this.xemSanPham = function (id) {
    var product = axios({
      url: `https://63e67404c8839ccc2858df76.mockapi.io/sanpham/${id}`,
      method: "GET",
    });
    return product;
  };
  this.capNhatSanPham = function (id, sanpham) {
    var product = axios({
      url: `https://63e67404c8839ccc2858df76.mockapi.io/sanpham/${id}`,
      method: "PUT",
      data: sanpham,
    });
    return product;
  };
}
